let url = 'http://127.0.0.1:8100/ftx'

export async function loadAPI() {
  let res = await fetch(url)
  let json = await res.json()
  if (json.success) {
    return json.result
  }
  return new Error(json)
}
