import React, { useEffect, useState } from 'react'
import './App.css'
import { io } from 'socket.io-client'
import { useEvent, dispatch } from 'react-use-event'

let socket = io('http://127.0.0.1:8100')
socket.on('ftx', data => {
  dispatch('ftx', { data })
})

type FTXEvent = {
  type: 'ftx'
  data: any[]
}

function App() {
  const [data, setData] = useState<any[]>([])
  useEvent<FTXEvent>('ftx', event => setData(event.data))
  return <div className="App">{JSON.stringify(data, null, 2)}</div>
}

export default App
