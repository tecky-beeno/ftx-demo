import express from 'express'
import http from 'http'
import cors from 'cors'
import expressSession from 'express-session'
import * as runningAt from 'listening-on'
import { join, resolve } from 'path'
import { config } from 'dotenv'
import SocketIO from 'socket.io'
import { router } from './router'
import { loadAPI } from './api'
config()

const app = express()
app.use(cors())
const server = http.createServer(app)
const io = new SocketIO.Server(server, {
  cors: {
    origin: '*',
  },
})

app.use(express.static('public'))

app.use(express.json() as any)
app.use(express.urlencoded({ extended: true }) as any)

const session = expressSession({
  secret: process.env.SESSION_SECRET || Math.random().toString(36),
  saveUninitialized: true,
  resave: true,
})
app.use(session)

app.use((req, res, next) => {
  if (req.method === 'GET') {
    console.log(req.method, req.url)
  } else {
    console.log(req.method, req.url, req.body)
  }
  next()
})

app.use(router)

let data = loadAPI()
data.then(onData)

function onData(data: any) {
  io.emit('ftx', data)
  setTimeout(() => {
    data = loadAPI()
    data.then(onData)
  }, 10 * 1000)
}

io.on('connection', socket => {
  data.then(data => socket.emit('ftx', data))
})

router.get('/ftx', (req, res) => {
  data.then(data => res.json(data))
})

app.use((req, res) => {
  res.status(404).sendFile(resolve(join('public', '404.html')))
})

const PORT = +process.env.PORT! || 8100

server.listen(PORT, () => {
  runningAt.print(PORT)
})
