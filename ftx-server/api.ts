import fetch from 'node-fetch'
import debug from 'debug'

let log = debug('api.ts')
log.enabled = true

export async function loadAPI() {
  let url = 'https://ftx.com/api/markets'
  log('load API from:', url)
  let res = await fetch(url)
  let json = await res.json()
  if (json.success) {
    return json.result
  }
  throw new Error(json)
}
